import utils from "./utils.js"
import md from "./snarkdown.js"
import hl from "lolight"

export function emojis(content) {
	return content.replace(/&lt;(a?):(.+?):(\d+)&gt;/g, (match, a, name, id) => {
		// a is animated, so gif, else png
		const format = a === "a" ? "gif" : "png"

		return `<img src="https://cdn.discordapp.com/emojis/${id}.${format}"
					 alt="${utils.escapeHTML(name)}" class="emoji" />`
	})
}

// escapes HTML, parses mentions and emojis
export function messageContent(message, userId) {
	let content = md.parse(utils.escapeHTML(message.content))

	// Parse mentions
	content = content.replace(/&lt;@!?(\d+)&gt;/g, (match, id) => {
		let mention = message.mentions.find(user => {
			return user.id === id
		})

		if (!mention) {
			return match
		}

		let c = `mention ${mention.id === userId ? "self" : ""}`
		return `<span class="${c}">${utils.escapeHTML(mention.username)}</span>`
	})

	// TODO: parse channels and roles

	// As we parse things, the string gets longer. Whatever is
	// longer should be parsed later.

	// Parse emojis
	content = emojis(content)

	return content
}

export function preview(a) {
	// Determine if a preview could be added
	switch (a.filename.toLowerCase().split(".").pop()) {
		case "png": case "jpg": case "jpeg": // image
			return `<a class="attachment" target="_blank" href="${a.url}"
				   ><img class="attachment preview" 
						   src="${a.proxy_url}" />
					</a>`
		case "mkv": case "mp4": case "webm":
			return `<video class="attachment preview" controls
						   src="${a.proxy_url}" />`
		case "mp3": case "ogg": case "opus": case "flac":
			return `<audio class="attachment preview" controls
						   src="${a.proxy_url}"
						   style="border-radius: 100px" />`
		default:
			return `<a class="attachment" target="_blank" href="${a.url}"
					>${utils.escapeHTML(a.filename)}</a>`
	}
}

export function time(time) {
	return new Date(time).toLocaleString("en", {
		day: "numeric",
		month: "short",
		hour: "2-digit",
		minute: "2-digit",
	})
}

export function code(code) {
	console.log(hl)
	return code
	let html = ''
	hl.tokenize(code).forEach(t => {
		html += `<span class="ll=${t[0]}">${utils.escapeHTML(t[1])}</span>`
	})

	return html
}

export default {
	time,
	emojis,
	messageContent,
	preview,
	code,
}
