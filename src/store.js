import Navaid from "navaid"
import { writable } from "svelte/store"

export const router = writable(Navaid("/"))
export const page = writable(null)
export const discord = writable(null)
export const token = writable("")
export const channelID = writable("")
export const guildID = writable("")
