/* Copyright (c) 2017 Jason Miller. All rights reserved.
 * This work is licensed under the terms of the MIT license.  
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

import utils from "./utils.js"
import render from "./render.js"

const TAGS = {
	"*" : ["<em>", "</em>"],
	"_" : ["<em>", "</em>"],
	"**": ["<strong>", "</strong>"],
	"__": ["<u>", "</u>"],
	"~~": ["<s>","</s>"],
	"||": ["<span class=\"spoiler\">", "</span>"],
}

/** Outdent a string based on the first indented line's leading whitespace
 *	@private
 */
function outdent(str) {
	return str.replace(RegExp('^'+(str.match(/^(\t| )+/) || '')[0], 'gm'), '')
}

function encodeAttr(str) {
	return utils.escapeHTML(str + '')
}

/** Parse Markdown into an HTML String. */
export function parse(md) {
	const tokenizer = /(?:^``` *(\w*)\n([\s\S]*?)\n```$)|((?:(?:^|\n)>\s+.*)+)|(?:(?:^|\n)(?:[>*+-]|\d+\.)\s+.*)+|(?:`([^`].*?)`)|(__|\*\*|[_*]|~~|\|\|)|(https?:\/\S+(?:\.|:)\S+)/gm
	
	let context = [],
		out = '',
		last = 0,
		chunk, prev, token, inner, t

	function tag(token) {
		var desc = TAGS[token],
			end = context[context.length-1]==token
		if (!desc) return token
		if (!desc[1]) return desc[0]
		context[end?'pop':'push'](token)
		return desc[end|0]
	}

	function flush() {
		let str = ''
		while (context.length) str += tag(context[context.length-1])
		return str
	}

	md = md.replace(/^\n+|\n+$/g, '')

	while (token = tokenizer.exec(md)) {
		prev = md.substring(last, token.index)
		last = tokenizer.lastIndex
		chunk = ""

		if (prev.match(/[^\\](\\\\)*\\$/)) {
			// escaped
		} else if (token[2]) { // codeblocks
			chunk = `<pre class="code ${token[1].toLowerCase()}">
						${render.code(token[2])}
					 </pre>`
		} else if (token[3]) { // quoteblocks
			chunk = `<blockquote>
						${encodeAttr(parsetoken[3].replace(/^\s*>/gm, ''))}
					 </blockquote>`
		} else if (token[4]) { // inline `code`
			chunk = `<code>${encodeAttr(token[4])}</code>`
		} else if (token[5]) { // inline *em* **strong** etc
			chunk = tag(token[5] || '--')
		} else if (token[6]) {
			let url = encodeAttr(token[6])
			chunk = `<a href="${url}">${url}</a>`
		} else {
			chunk = encodeAttr(token[0])
		}

		out += prev
		out += chunk
	}

	return (out + md.substring(last) + flush()).trim()
}

export default {
	parse,
}
