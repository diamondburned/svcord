const tags = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': "&quot;",
}

export function escapeHTML(string) {
	return string.replace(/[&<>]/g, (tag) => {
		return tags[tag] || tag
	})
}

export function humanizeArray(array) {
	switch (array.length) {
	case 0:  return ""
	case 1:  return array[0]
	case 2:  return `${array[0]} and ${array[1]}`
	default: 
		let output = ""
		for (let i = 0; i < array.length-1; i++) {
			output += array[i] + " "
		}

		return output + "and " + array[array.length - 1]
	}
}

export function userAvatar(user) {
	return user.avatar ? 
		`https://cdn.discordapp.com/avatars/${user.id}/${user.avatar}.png` :
		"//:0"
}

export default {
	escapeHTML, humanizeArray, userAvatar,
}
